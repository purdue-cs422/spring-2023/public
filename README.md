<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 422: Computer Networks (Spring 2023)  

[[_TOC_]]
 
## Logistics
 
- Instructor: [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- Teaching assistants: 
  - [Rakin Haider](https://sites.google.com/site/rakinhaider/)
  - [Darwin Kamanuru](https://www.linkedin.com/in/darwin-k/)
- Lecture time: **TTh 4:30-5:45pm**
- Location: Stanley Coulter Hall 239	
- Credit Hours: 3.00
- Course discussion and announcements: [Campuswire](https://campuswire.com/c/G23E12A23) 
- Paper reviews: [Perusall](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/_/dashboard)
- Development environment: [AWS Academy](https://awsacademy.instructure.com/courses/36805)
- Exam submission: [Gradescope](https://www.gradescope.com/courses/483070)
- Office hours
  - Wednesday 2:30-3:30pm, [Zoom](https://purdue-edu.zoom.us/j/94712829690?pwd=eStYYVFIaHFaRWtCdmhocmVrY2l5dz09), Muhammad Shahbaz
  - Tuesday 3:00-4:00pm, [Zoom](https://purdue-edu.zoom.us/j/97353408358), Rakin Haider
  - Wednesday 11:30am-12:30pm, [Zoom](https://purdue-edu.zoom.us/my/darwk), Darwin Kamanuru
- Practice study observation (PSO), HAAS 257
  - Wednesday 4:30-5:20pm, Rakin Haider & Darwin Kamanuru
  - Thursday 10:30am-11:20am, Rakin Haider
  - Friday 1:30-2:20pm, Darwin Kamanuru

> **Note:** Visit [Brightspace](https://purdue.brightspace.com/d2l/home/703085) for instructions on joining [Campuswire](https://campuswire.com/c/G23E12A23), [Gradescope](https://www.gradescope.com/courses/483070), [Perusall](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/_/dashboard), and [AWS Academy](https://awsacademy.instructure.com/courses/36805).

#### Suggesting edits to the course page and more ...

We strongly welcome any changes, updates, or corrections to the course page or assignments or else that you may have. Please submit them using the [GitLab merge request workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).

## Course Description

CS 422 is an undergraduate-level course in Computer Networks at Purdue University. In this course, we will explore the underlying principles and design decisions that have enabled the Internet to (inter)connect billions of people and trillions of things on and off this planet---especially under the current circumstances marred by COVID-19. We will study the pros and cons of the current Internet design, ranging from classical problems (e.g., packet switching, routing, naming, transport, and congestion control) to emerging and future trends, like data centers, software-defined networking (SDN), programmable data planes, and network function virtualization (NFV) to name a few.

The goals for this course are:

- To become familiar with the classical and emerging problems in networking and their solutions.
- To learn what's the state-of-the-art in networking research: network architecture, protocols, and systems.
- To gain some practice in reading research papers and critically understanding others' research.
- To gain experience with network programming using industry-standard and state-of-the-art networking platforms.

# Course Syllabus and Schedule

> **Notes:** 
> - This syllabus and schedule is preliminary and subject to change.
> - Everything is due at 11:59 PM (Eastern) on the given day.
> - Abbreviations refer to the following:
>   - PD: Peterson/Davie (online version)
>   - KR: Kurose/Ross (6th edition)
>   - SDN: Peterson/Cascone/O’Connor/Vachuska/Davie (online version)
>   - 5G: Peterson/Sunay (online version)
>   - GV: George Varghese (1st edition)

| Date    | Topics  | Notes | Readings |
| :------ | :------ | :------  | :------ |
| **Week 1** | **Course Overview** | | |
| Tue <br> Jan 10 | | &bull; *No class* | &bull; [Internet History](https://www.internetsociety.org/internet/history-internet/brief-history-internet/) |
| Thu <br> Jan 12 | Introduction ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12047626/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6290143&srcou=703085)) | | &bull; PD: [1.1 - 1.2 (Applications, Requirements)](https://book.systemsapproach.org/foundation.html) |
| **Week 2** | **History of the Internet** | | |
| Tue <br> Jan 17 | A Brief History of the Internet ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12096261/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6335162&srcou=703085)) | | &bull; [How to Read a Paper](https://gitlab.com/purdue-cs422/spring-2023/public/-/raw/main/readings/HowToRead2017.pdf) |
| Thu <br> Jan 19 | [AWS Academy](https://gitlab.com/purdue-cs422/spring-2023/public/-/blob/main/assignments/cs422-awsacademy-hotwo.pdf) and [Assignment0](assignments/assignment0) Walkthrough | Instructors: [Rakin Haider](https://sites.google.com/site/rakinhaider/) and [Ertza Warraich](https://ertza.me/) | |
| **Week 3** | **Network Building Blocks and APIs** | | |
| Tue <br> Jan 24 | Layering and Protocols ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12105519/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6343805&srcou=703085)) | | &bull; PD: [1.3 (Architecture)](https://book.systemsapproach.org/foundation/architecture.html) <br> &bull; [End-to-End Arguments](https://gitlab.com/purdue-cs422/spring-2022/public/-/raw/main/readings/e2eArgument84.pdf) (Optional) |
| Thu <br> Jan 26 | Sockets: The Network Interface ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12118683/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6359301&srcou=703085)) | &bull; [Demo](demos/Sockets) | &bull; PD: [1.4 (Software)](https://book.systemsapproach.org/foundation/software.html) <br> &bull; [Beej's Guide](http://beej.us/guide/bgnet/) (Optional) |
| **Week 4** | **Local Area Networks I** | | |
| Tue <br> Jan 31 | Direct Links: The Wire Interface ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12130665/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6371171&srcou=703085)) | &bull; [Paper Review 1](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/cerf74-213630560?assignmentId=yNTx6BGFKNGbu7MEq&part=1) `due Mar 07` <br> &bull; [Assignment 1](assignments/assignment1) `due Feb 13` | &bull; PD: [2.1 - 2.6 (Technology, Encoding, Framing, ...)](https://book.systemsapproach.org/direct.html) |
| Wed <br> Feb 01 | | &bull; [Quiz 1](https://www.gradescope.com/courses/483070/assignments/2627975/submissions) `due Feb 02` | |
| Thu <br> Feb 02 | Indirect Links: Internetworking (L2/L3) ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12151556/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6394776&srcou=703085)) | | &bull; PD: [3.2 - 3.3 (Switched Ethernet, Internet)](https://book.systemsapproach.org/internetworking.html) |
| **Week 5** | **Local Area Networks II** | | |
| Tue <br> Feb 07 | Indirect Links: Internetworking Protocols ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12160701/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6404048&srcou=703085)) | &bull; [Demo](demos/ARP) | &bull; PD: [3.3.1, 3.3.2, 3.3.6 (Inernetwork, Service Model, ARP)](https://book.systemsapproach.org/internetworking.html) |
| Thu <br> Feb 09 | Direct Links: The Wireless Interface | &bull; *No class*  | &bull; PD: [2.7 - 2.8 (Wireless and Access Networks)](https://book.systemsapproach.org/direct.html) |
| **Week 6** | **Network Addressing and Configuration** | | |
| Tue <br> Feb 14 | Flat/Classful Addressing: Ethernet, IP, and Subnets ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12189442/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6432736&srcou=703085)) | | &bull; PD: [3.3.3 - 3.3.9 (Addressing, DHCP ...)](https://book.systemsapproach.org/internetworking.html) |
| Thu <br> Feb 16 | Classless Addressing: CIDR ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12200144/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6444186&srcou=703085)) | | &bull; PD: [3.3.5 (Classless Addressing)](https://book.systemsapproach.org/internetworking.html) |
| **Week 7** | **Process-to-Process Communication** | | |
| Tue <br> Feb 21 | Transport: Reliable Delivery ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12405497/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6462216&srcou=703085)) | &bull; [Assignment 2](assignments/assignment2) `due Mar 08` | &bull; PD: [2.5 (Reliable Transmission)](https://book.systemsapproach.org/direct/reliable.html) <br> &bull; PD: [5.1 - 5.2 (UDP, TCP)](https://book.systemsapproach.org/e2e.html) |
| Wed <br> Feb 22 | | &bull; [Quiz 2](https://www.gradescope.com/courses/483070/assignments/2689960/submissions) `due Feb 23` | |
| Thu <br> Feb 23 | Transport: Flow Control ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12423619/View)) | | &bull; PD: [5.2 (TCP)](https://book.systemsapproach.org/e2e.html) <br> &bull; PD: [5.3 - 5.4 (RPC, RTP)](https://book.systemsapproach.org/e2e.html) (Optional)  |
| **Week 8** | **Software-Defined Networks and Data Centers** | | |
| Tue <br> Feb 28 | Control-Plane Abstractions and OpenFlow ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12432429/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6488069&srcou=703085)) | | &bull; How SDN will Shape Networking ([video](https://www.youtube.com/watch?v=c9-K5O_qYgA)) <br> &bull; SDN: [3: Basic Architecture](https://sdn.systemsapproach.org/arch.html) |
| Thu <br> Mar 02 | Origin and Architectures of Data Centers ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12450823/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6506608&srcou=703085)) | | &bull; [MapReduce](https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf) and an interview with Jeff Dean and Sanjay Ghemawat on why it came into being ([video](https://youtu.be/NXCIItzkn3E)) |
| **Week 9** | **Wide Area Networks I** | | |
| Tue <br> Mar 07 | Direct Networks: Intradomain Routing ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12458376/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6514093&srcou=703085)) | | &bull; PD: [3.4.2 - 3.4.3 (Routing: Distance Vector and Link State)](https://book.systemsapproach.org/internetworking/routing.html) |
| Thu <br> Mar 09 | Indirect Networks: Interdomain Routing ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12481402/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6537582&srcou=703085)) | | &bull; PD: [4.1 (Global Internet)](https://book.systemsapproach.org/scaling/global.html) |
| Sat <br> Mar 11 | | &bull; [Paper Review 2](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/sdnhistory?assignmentId=LPPLkEGxRyMbqCqQv&part=1) `due Apr 23` | | 
| **Week 10** | **Spring Break** | | |
| **Week 11** | **Wide Area Networks II** | | |
| Tue <br> Mar 21 | Indirect Networks: Peering and IXPs ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12490298/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657921&srcou=703085)) | &bull; [Assignment 3](assignments/assignment3) `due Apr 06` | &bull; KR: 4.6.3 (Inter-AS Routing: BGP) <br> &bull; [Where is Internet Congestion Occuring?](https://medium.com/network-insights/where-is-internet-congestion-occurring-e4333ed71168) |
| Wed <br> Mar 22 | | &bull; [Quiz 3](https://www.gradescope.com/courses/483070/assignments/2761511/submissions) `due Mar 23` | |
| Thu <br> Mar 23 | Planet-Scale Applications and Traffic Characteristics ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12501623/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657922&srcou=703085)) | | |
| **Week 12** | **Midterm Week** | | |
| Tue <br> Mar 28 | *Midterm Prep* | | |
| Thu <br> Mar 30 | *Midterm Exam* | | |
| **Week 13** | **Programmable Networks (and Network Data Planes)** | | |
| Tue <br> Apr 04 | Protocol-Independent Switching: Bottoms Up vs Top Down ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/12987564/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657923&srcou=703085)) | | &bull; SDN: [4 (Bare-Metal Switches)](https://sdn.systemsapproach.org/switch.html) |
| Thu <br> Apr 06 | Router Design: Lookup and Scheduling ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13005166/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657924&srcou=703085)) | | &bull; KR: 4.3 (What's Inside a Router?) <br> &bull; GV: [10 (Exact-Match Lookups), 11 (Prefix-Match Lookups), 13 (Switching)](https://purdue.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma99169139049601081&context=L&vid=01PURDUE_PUWL:PURDUE&lang=en&search_scope=MyInst_and_CI&adaptor=Local%20Search%20Engine&tab=Everything&query=any,contains,george%20varghese) |
| **Week 14** | **Resource Allocation** | | |
| Tue <br> Apr 11 | Congestion Control and Queuing Disciplines ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13016881/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657925&srcou=703085)) | &bull; [Assignment 4](assignments/assignment4) `due Apr 23` | &bull; PD: [6.1 - 6.2 (Issues, Queuing)](https://book.systemsapproach.org/congestion.html) |
| Thu <br> Apr 13 | Transport: Congestion Control (TCP) ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13034152/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657926&srcou=703085)) | | &bull; PD: [6.3 - 6.4 (TCP, ...)](https://book.systemsapproach.org/congestion.html) |
| **Week 15** | **Network Applications** | | |
| Tue <br> Apr 18 | The World Wide Web (HTTP) ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13048008/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657927&srcou=703085)) | | &bull; PD: [9.1.2 (World Wide Web)](https://book.systemsapproach.org/applications/traditional.html#world-wide-web-http) |
| Wed <br> Apr 19 | | &bull; [Quiz 4](https://www.gradescope.com/courses/483070/assignments/2834598/submissions) `due Apr 20` | |
| Thu <br> Apr 20 | Infrastructure Services & Overlay Networks ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13060256/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6657928&srcou=703085)) | | &bull; PD: [9.3.1, 9.4.1 (DNS, Overlays)](https://book.systemsapproach.org/applications.html) |
| **Week 16** | **Parting Thoughts & Course Summary** | | |
| Tue <br> Apr 25 | Research Papers Discussion ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13097326/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6688435&srcou=703085)) | | |
| Thu <br> Apr 27 | *Final Review* ([ppt](https://purdue.brightspace.com/d2l/le/content/703085/viewContent/13097327/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=703085&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-6688436&srcou=703085)) | | |
| **Week 17** | **Exam Week** | | |
| Thu <br> May 04 | *Final Exam* | &bull; Time and place: **1:00pm in BRNG 2280** | |

## Prerequisites

This course assumes that students have a basic understanding of data structures and algorithms and experience with programming languages like C/C++ and Python. Please see [CS 240](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=24000), [CS 380](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=38003), or similar courses at Purdue for reference.

## Recommended Textbooks
- Computer Networking: A Top-Down Approach by J. Kurose and K. Ross (6th Edition)
- Computer Networks: A Systems Approach by L. Peterson and B. Davie ([Online Version](https://book.systemsapproach.org/index.html))
- Software-Defined Networks: A Systems Approach by L. Peterson, C. Cascone, B. O’Connor, T. Vachuska, and Bruce Davie ([Online Version](https://sdn.systemsapproach.org/index.html))
- 5G Mobile Networks: A Systems Approach by L. Peterson and O. Sunay ([Online Version](https://5g.systemsapproach.org/index.html))

> Other optional but interesting resources: [Sytems Approach - Blog](https://www.systemsapproach.org/blog), [TCP Congestion Control: A Systems Approach](https://tcpcc.systemsapproach.org/index.html), [Operating an Edge Cloud: A Systems Approach](https://ops.systemsapproach.org), and [Network Algorithmics: An Interdisciplinary Approach to Designing Fast Networked Devices](https://purdue.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma99169139049601081&context=L&vid=01PURDUE_PUWL:PURDUE&lang=en&search_scope=MyInst_and_CI&adaptor=Local%20Search%20Engine&tab=Everything&query=any,contains,george%20varghese)

## Paper Reading and Discussion

During the course, we will be reading and discussing **two** research papers on topics ranging from network protocols, systems, and architectures. You should closely read each paper and add comments and questions along with a summary (5 lines or so) of the paper on [Perusall](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/_/dashboard) by the due date below. Please plan to provide at least five comments or questions for each paper on Perusall and follow the comments from other students and the course staff. 

> **Note:** General tips on reading and reviewing papers are [here](reading-papers.md). 

Grades for your class participation and paper reviews will be determined based on attendance and, more importantly, contributions to paper discussions on Perusall.

### Reading list

- [Paper 1: A Protocol for Packet Network Intercommunication](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/cerf74-213630560?assignmentId=yNTx6BGFKNGbu7MEq&part=1) `due Mar 07`
- [Paper 2: The Road to SDN: An Intellectual History of Programmable Networks](https://app.perusall.com/courses/spring-2023-cs-42200-le1-lec/sdnhistory?assignmentId=LPPLkEGxRyMbqCqQv&part=1) `due Apr 23`

## Programming Assignments

- [Assignment 0: Virtual Networks using Mininet and ONOS](assignments/assignment0) `not graded`
- [Assignment 1: File and Message Transmission using Sockets](assignments/assignment1) `due Feb 13`
- [Assignment 2: From Bridging to Switching with VLANs](assignments/assignment2) `due Mar 08`
- [Assignment 3: DNS Reflection Attacks' Detection and Mitigation](assignments/assignment3) or Special Assignment using ChatGPT `due Apr 06`
- [Assignment 4: From Bridging to Switching using P4's Match-Action Tables (MATs)](assignments/assignment4) `due Apr 23`

> **See the list of submitted ChatGPT projects [here](projects).**

## Quizzes

- [Quiz 1: Topics from weeks 1-3](https://www.gradescope.com/courses/483070/assignments/2627975/submissions) `due Feb 02`
- [Quiz 2: Topics from weeks 4-6](https://www.gradescope.com/courses/483070/assignments/2689960/submissions) `due Feb 23`
- [Quiz 3: Topics from weeks 7-9](https://www.gradescope.com/courses/483070/assignments/2761511/submissions) `due Mar 23`
- [Quiz 4: Topics from weeks 10-14](https://www.gradescope.com/courses/483070/assignments/2834598/submissions) `due Apr 20`

> **Format:** take home, open book

## Midterm and Final Exams
There will be one midterm and a final exam based on course content (lectures, assignments, and paper readings).

- Midterm Exam `on Mar 30` 
- Final Exam `on May 04 @ 1:00pm in BRNG 2280`

> **Format:** in class, open book

## Grading

- Class participation and discussions: 10%
- Programming assignments: 40%
- Quizzes: 17.5%
- Midterm exam: 12.5%
- Final exam: 20%

## Policies

### Late submission

- Grace period: 24 hours for the entire semester.
- After the grace period, 25% off for every 24 hours late, rounded up.

If you have extenuating circumstances that result in an assignment being late, please let us know about them as soon as possible.

### Academic integrity

We will default to Purdue's academic policies throughout this course unless stated otherwise. You are responsible for reading the pages linked below and will be held accountable for their contents.
- http://spaf.cerias.purdue.edu/integrity.html
- http://spaf.cerias.purdue.edu/cpolicy.html

### Honor code

By taking this course, you agree to take the [Purdue Honors Pledge](https://www.purdue.edu/odos/osrr/honor-pledge/about.html): "As a boilermaker pursuing academic excellence, I pledge to be honest and true in all that I do. Accountable together - we are Purdue."

### COVID-19 and quarantine + isolation!

Please visit [Protect Purdue Plan](https://protect.purdue.edu/plan/) or [Spring 2023 resources](https://www.purdue.edu/innovativelearning/teaching-remotely/resources.aspx) for most up-to-date guidelines and instructions.

## Acknowledgements

This class borrows inspirations from several incredible sources.

- The course syllabus page format loosely follows Xin Jin's [EN.601.414/614](https://github.com/xinjin/course-net) class at John Hopkins.
- The lecture slides' material is partially adapted from my Ph.D. advisors, Jen Rexford's [COS 461](https://www.cs.princeton.edu/courses/archive/fall20/cos461) class and Nick Feamster's [COS 461](https://www.cs.princeton.edu/courses/archive/spring19/cos461/) class at Princeton.
