<img src="../others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 422 ChatGPT Projects @ Purdue University (Spring 2023)

1. [**Building a Small Virtual Network inside ChatGPT** (Medium)](https://medium.com/@v-enkatk/building-a-small-virtual-network-inside-chatgpt-4b6722766125)
    - Venkat Kunaparaju
1. [**Building a Learning Switch with ChatGPT** (Medium)](https://medium.com/@carolineeausema/building-a-learning-switch-with-chatgpt-505d0196a9e7)
    - Caroline Ausema, Katya Yegorova, and Frances O'Leary
1. [**DNS Reflection Attacks' Detection and Mitigation with ChatGPT** (PDF)](https://gitlab.com/purdue-cs422/spring-2023/public/-/raw/main/projects/3.pdf)
    - Jianjun Lang and Winnie Li
1. [**Training ChatGPT to Act Like a Learning Switch** (PDF)](https://gitlab.com/purdue-cs422/spring-2023/public/-/raw/main/projects/4.pdf)
    - Riley Henslee and Jagger Huff
1. [**Simulate a Learning Switch within ChatGPT** (PDF)](https://gitlab.com/purdue-cs422/spring-2023/public/-/raw/main/projects/5.pdf)
    - Derek Lee
1. [**Making ChatGPT Behave like a Junos OS CLI** (PDF)](https://gitlab.com/purdue-cs422/spring-2023/public/-/raw/main/projects/6.pdf)
    - Kyung Jae Pae
